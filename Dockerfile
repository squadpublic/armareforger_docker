FROM cm2network/steamcmd:latest

ENV STEAMAPPID 1874900
ENV STEAMAPP armaReforgerServer
ENV STEAMAPPDIR "${HOMEDIR}/${STEAMAPP}-dedicated"



RUN set -x

# Switch to user
USER ${USER}

WORKDIR ${HOMEDIR}

COPY entry.sh .
COPY serverconfig.json ${STEAMAPPDIR}/
CMD ["bash", "entry.sh"]